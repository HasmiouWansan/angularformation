import { Component,Input, OnInit } from '@angular/core';
import {AppareilServices} from '../services/appareil.services';

@Component({
  selector: 'app-appareils',
  templateUrl: './appareils.component.html',
  styleUrls: ['./appareils.component.scss']
})
export class AppareilsComponent implements OnInit {
  @Input() appareilName: string;
  @Input() appareilStatus: string;
  @Input() appareilStatusColor: string;
  @Input() appareilIcon;
  @Input() lastUsed:Date;
  @Input() indexOfAppareil;
  @Input() id;
  @Input() isSwitchOn;

  control(i = this.indexOfAppareil){
    (this.isSwitchOn) ? this.switchOff(i) : this.switchOn(i);
  }
  switchOn(i) {
    this.appareilService.switchOn(i);
  }

  switchOff(i) {
    this.appareilService.switchOff(i);
  }

  constructor(private appareilService: AppareilServices ) { }

  ngOnInit() {
  }

  /*getStatus(){
    return this.appareilStatus;
  }*/

}


