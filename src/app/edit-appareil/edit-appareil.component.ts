import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {AppareilServices} from '../services/appareil.services';
import {Router} from '@angular/router';

@Component({
  selector: 'app-edit-appareil',
  templateUrl: './edit-appareil.component.html',
  styleUrls: ['./edit-appareil.component.scss']
})
export class EditAppareilComponent implements OnInit {

  defaultStatus = 'off';

  constructor(private appareilService: AppareilServices, private router: Router) { }

  ngOnInit() {
  }

  onSubmit(f: NgForm) {
    const name = f.value['name'];
    const status = f.value['status'];
    const icon = f.value['icon'];

    this.appareilService.addAppareil(name, status, icon);
    this.router.navigate(['/appareils']);
  }

}
