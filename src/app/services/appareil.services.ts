import { Subject, from } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class AppareilServices {

  constructor(private httpClient: HttpClient) { }

  // appareilSubject = new Subject<any[]>();
  // private appareils = [
  appareils = [
    new Appareil(1, 'Téléphone', 'off', 'fa fa-phone'),
    new Appareil(2, 'Télévision', 'on', 'fa fa-tv'),
    new Appareil(3, 'Computer', 'off', 'fa fa-laptop'),
    new Appareil(4, 'Xbox One', 'on', 'fa fa-gamepad'),
  ];

  /*createDatas(){
    this.appareils = [
      new Appareil(1,'Téléphone', 'off', 'fa fa-phone'),
      new Appareil(2,'Télévision', 'on', 'fa fa-tv'),
      new Appareil(3,'Computer', 'off','fa fa-laptop'),
      new Appareil(4,'Xbox One', 'on','fa fa-gamepad'),
    ];
    return this.appareils;
  }*/

  /*emitAppareilSubject() {
    this.appareilSubject.next(this.appareils.slice());
  }*/

  addAppareil(name: string, status: string, icon: string) {
    var id = this.appareils.length + 1;
    console.log(id);
    this.appareils.push(new Appareil(id, name, status, icon));
    // this.emitAppareilSubject();
  }

  getAppareilById(id: number) {
    const app = this.appareils.find(
      (a) => {
        return a.getId() === id;
      }
    )
    return app;
  }

  switchOnAll() {
    this.appareils.forEach(a => {
      a.setStatus('On');
    });
    // this.emitAppareilSubject();
  }

  switchOffAll() {
    for (let a of this.appareils) {
      a.setStatus('Off');
      // this.emitAppareilSubject();
    }
  }

  switchOn(index: number) {
    this.appareils[index].setStatus('On');
    // this.emitAppareilSubject();
  }

  switchOff(index: number) {
    this.appareils[index].setStatus('Off');
    // this.emitAppareilSubject();
  }

  saveToServer() {
    //console.log(JSON.stringify(this.appareils));
    this.httpClient
      .put('https://httpclientdemo-29e51.firebaseio.com/appareils.json', JSON.stringify(this.appareils))
      .subscribe(
        () => {
          console.log('Enregistrement terminé !');
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }

  getFromServer() {
    this.httpClient
      .get<any[]>('https://httpclientdemo-29e51.firebaseio.com/appareils.json')
      .subscribe(
        (response) => {
          console.log(response);
          this.appareils = response;
          //this.emitAppareilSubject();
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }

}

class Appareil {
  private id: number;
  private icon: string;
  private name: string;
  private status: string;
  private lastUsed: Date;

  constructor(id, name, status, icon) {
    this.setName(name);
    this.setStatus(status);
    this.lastUsed = new Date();
    this.icon = icon;
    this.id = id;
  }

  getId() {
    return this.id;
  }

  getName() {
    return this.name;
  }

  setName(n: string) {
    this.name = n;
  }

  getStatus() {
    return this.status;
  }

  setStatus(s: string) {
    this.status = s;
  }

  getIcon() {
    return this.icon;
  }

  setIcon(i: string) {
    this.icon = i;
  }

  getLastUsed() {
    return this.lastUsed;
  }

  setLasUsed(d: Date) {
    this.lastUsed = d;
  }

  getStatusColor(): string {
    return (this.getStatus() === 'off') ? 'red' : 'green';
  }

  isSwitchOn() {
    return (this.status === 'on') ? true : false;
  }
}

