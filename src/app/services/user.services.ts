import User from '../models/Users.model';
import { Subject } from 'rxjs';

export class UserService {
    private users: User[] = [{
        firstName: "Hasmiou",
        lastName: "Diallo",
        email: "hasmiou@mail.com",
        drinkPreference: "Orangina",
        hobbies: [
            "coder",
            "Cinema",
            "Game",
        ]
    }
    ];
    userSubject = new Subject<User[]>();

    emitUser() {
        this.userSubject.next(this.users.slice());
    }

    addUser(user: User) {
        this.users.push(user);
        this.emitUser();
    }
}