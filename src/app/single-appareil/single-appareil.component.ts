import { Component, OnInit } from '@angular/core';
import {AppareilServices} from '../services/appareil.services';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-single-appareil',
  templateUrl: './single-appareil.component.html',
  styleUrls: ['./single-appareil.component.scss']
})
export class SingleAppareilComponent implements OnInit {

    // 'id': string,
    'name': string;
    'icon': string;
    'status': string;
    'lastUsed': Date;
  param :any;
  constructor(private appareilService: AppareilServices, private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.param = this.activeRoute.snapshot.params;
    const id = this.param['id'];
    const appareilObject = this.appareilService.getAppareilById(+id);
    this.name = appareilObject.getName();
    this.icon = appareilObject.getIcon();
    this.status = appareilObject.getStatus();
    this.lastUsed = appareilObject.getLastUsed();
  }

}
