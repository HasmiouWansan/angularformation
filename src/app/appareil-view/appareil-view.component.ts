import { Component, OnDestroy, OnInit } from '@angular/core';
import { AppareilServices } from '../services/appareil.services';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-appareil-view',
  templateUrl: './appareil-view.component.html',
  styleUrls: ['./appareil-view.component.scss']
})
// export class AppareilViewComponent implements OnInit, OnDestroy {
export class AppareilViewComponent implements OnInit {

  isAuth = false;

  appareils: any;
  // appareilSubscription: Subscription;

  private lastUpdate = new Promise(
    (resolve, reject) => {
      const date = new Date();
      setTimeout(
        () => {
          resolve(date);
        }, 3000
      );
    }
  );

  onAllumer() {
    this.appareilService.switchOnAll();
  }

  onEteindre() {
    this.appareilService.switchOffAll();
  }

  onSave() {
    this.appareilService.saveToServer();
  }

  onLoad() {
    this.appareilService.getFromServer();
  }

  getLastUpdate() {
    return this.lastUpdate;
  }


  constructor(private appareilService: AppareilServices) {
    setTimeout(
      () => {
        this.isAuth = true;
      }, 5000
    );
  }

  ngOnInit(): void {
    // this.appareils = this.appareilService.createDatas();
    this.appareils = this.appareilService.appareils;
    this.onLoad();
    /*this.appareils = this.appareilService.appareilSubject.subscribe(
      (appareils: any[]) => {
        this.appareils = appareils;
    });
    this.appareilService.emitAppareilSubject();*/
  }

  /*ngOnDestroy(): void {
    this.appareilSubscription.unsubscribe();
  }*/

}
